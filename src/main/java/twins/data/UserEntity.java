package twins.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="USERS")
public class UserEntity {
	private String space;
	private String email;
	private UserRole role;
	private String username;
	private String avatar;

	public UserEntity() {

	}

	public UserEntity(String space, String email, UserRole role, String username, String avatar) {
		super();
		this.space = space;
		this.email = email;
		this.role = role;
		this.username = username;
		this.avatar = avatar;
	}

	
	//Getters
	
	public String getSpace() {
		return space;
	}

	public String getEmail() {
		return email;
	}

	public UserRole getRole() {
		return role;
	}

	public String getUsername() {
		return username;
	}

	public String getAvatar() {
		return avatar;
	}

	//Setters
	
	public void setSpace(String space) {
		this.space = space;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	//PK
	@Id
	@Column(name="ID")
	public String getKey() {
		return this.space + "=" + this.email;
	}
	
	public void setKey(String key) {
		String[] splitResult = key.split("=");
		this.space = splitResult[0];
		this.email = splitResult[1];
	}
}
