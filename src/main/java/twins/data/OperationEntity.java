package twins.data;

import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class OperationEntity {
	
	private String operationSpace;
	private String operationId;
	private String type;
	private String itemSpace;
	private String itemId;
	private Date createdTimeStamp;
	private String userSpace;
	private String userEmail;
	//private Map<String,Object> operationsAttributes;
	private String operationsAttributes;
	
	public OperationEntity() {

	}

//	public OperationEntity(String operationSpace, String operationId, String type, String itemSpace, String itemId,
//			Date createdTimeStamp, String userSpace, String userEmail,
//			Map<String, Object> operationsAttributes) {
//		super();
//		this.operationSpace = operationSpace;
//		this.operationId = operationId;
//		this.type = type;
//		this.itemSpace = itemSpace;
//		this.itemId = itemId;
//		this.createdTimeStamp = createdTimeStamp;
//		this.userSpace = userSpace;
//		this.userEmail = userEmail;
//		this.operationsAttributes = operationsAttributes;
//	}
	public OperationEntity(String operationSpace, String operationId, String type, String itemSpace, String itemId,
			Date createdTimeStamp, String userSpace, String userEmail,
			String operationsAttributes) {
		super();
		this.operationSpace = operationSpace;
		this.operationId = operationId;
		this.type = type;
		this.itemSpace = itemSpace;
		this.itemId = itemId;
		this.createdTimeStamp = createdTimeStamp;
		this.userSpace = userSpace;
		this.userEmail = userEmail;
		this.operationsAttributes = operationsAttributes;
	}

	//Getters

	public String getOperationSpace() {
		return operationSpace;
	}

	public String getOperationId() {
		return operationId;
	}

	public String getType() {
		return type;
	}

	public String getItemSpace() {
		return itemSpace;
	}

	public String getItemId() {
		return itemId;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="OPERATION_CREATION")
	public Date getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public String getUserSpace() {
		return userSpace;
	}

	public String getUserEmail() {
		return userEmail;
	}
//	@Lob
//	public Map<String, Object> getOperationsAttributes() {
//		return operationsAttributes;
//	}
	@Lob
	public String getOperationsAttributes() {
		return operationsAttributes;
	}

	//Setters

	public void setOperationSpace(String operationSpace) {
		this.operationSpace = operationSpace;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setItemSpace(String itemSpace) {
		this.itemSpace = itemSpace;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public void setCreatedTimeStamp(Date createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	public void setUserSpace(String userSpace) {
		this.userSpace = userSpace;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

//	public void setOperationsAttributes(Map<String, Object> operationsAttributes) {
//		this.operationsAttributes = operationsAttributes;
//	}
	public void setOperationsAttributes(String operationsAttributes) {
		this.operationsAttributes = operationsAttributes;
	}
	
	@Id
	@Column(name="ID")
	public String getKey() {
		return this.operationSpace + "=" + this.operationId;
	}
	
	public void setKey(String key) {
		String[] splitResult = key.split("=");
		this.operationSpace = splitResult[0];
		this.operationId = splitResult[1];
	}
	

}

