package twins.data;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
public class ItemEntity {

	private String itemSpace;
	private String itemId;
	private String type;
	private String name;
	private boolean active;
	private Date createdTimeStamp;
	private String creatorSpace;
	private String creatorEmail;
	private double lat;
	private double lng;
	//private Map<String,Object> itemAttributes;
	private String itemAttributes;
	
	public ItemEntity() {

	}

//	public ItemEntity(String itemSpace, String itemId, String type, String name, boolean active,
//			Date createdTimeStamp, String creatorSpace, String creatorEmail, double lat, double lng,
//			Map<String, Object> itemAttributes) {
//		super();
//		this.itemSpace = itemSpace;
//		this.itemId = itemId;
//		this.type = type;
//		this.name = name;
//		this.active = active;
//		this.createdTimeStamp = createdTimeStamp;
//		this.creatorSpace = creatorSpace;
//		this.creatorEmail = creatorEmail;
//		this.lat = lat;
//		this.lng = lng;
//		this.itemAttributes = itemAttributes;
//	}
	
	public ItemEntity(String itemSpace, String itemId, String type, String name, boolean active,
			Date createdTimeStamp, String creatorSpace, String creatorEmail, double lat, double lng,
			String itemAttributes) {
		super();
		this.itemSpace = itemSpace;
		this.itemId = itemId;
		this.type = type;
		this.name = name;
		this.active = active;
		this.createdTimeStamp = createdTimeStamp;
		this.creatorSpace = creatorSpace;
		this.creatorEmail = creatorEmail;
		this.lat = lat;
		this.lng = lng;
		this.itemAttributes = itemAttributes;
	}

	//Getters
	public String getItemSpace() {
		return itemSpace;
	}

	public String getItemId() {
		return itemId;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public boolean getActive() {
		return active;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ITEM_CREATION")
	public Date getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public String getCreatorSpace() {
		return creatorSpace;
	}

	public String getCreatorEmail() {
		return creatorEmail;
	}

	public double getLat() {
		return lat;
	}

	public double getLng() {
		return lng;
	}
//	@Lob
//	public Map<String, Object> getItemAttributes() {
//		return itemAttributes;
//	}
	@Lob
	public String getItemAttributes() {
		return itemAttributes;
	}
	
	
	//Setters
	public void setItemSpace(String itemSpace) {
		this.itemSpace = itemSpace;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setCreatedTimeStamp(Date createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	public void setCreatorSpace(String creatorSpace) {
		this.creatorSpace = creatorSpace;
	}

	public void setCreatorEmail(String creatorEmail) {
		this.creatorEmail = creatorEmail;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

//	public void setItemAttributes(Map<String, Object> itemAttributes) {
//		this.itemAttributes = itemAttributes;
//	}
	public void setItemAttributes(String itemAttributes) {
		this.itemAttributes = itemAttributes;
	}
	
	//PK
	@Id
	@Column(name="ID")
	public String getKey() {
		return this.itemSpace + "=" + this.itemId;
	}
	
	public void setKey(String key) {
		String[] splitResult = key.split("=");
		this.itemSpace = splitResult[0];
		this.itemId = splitResult[1];
	}
	

	
}