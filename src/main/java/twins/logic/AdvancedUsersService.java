package twins.logic;

import java.util.List;

import twins.controllers.UserBoundary;

public interface AdvancedUsersService extends UsersService {
	public List <UserBoundary> getAllUsers(String adminSpace,String adminEmail,int size,int page);

}
