package twins.logic;

import java.util.List;

import twins.controllers.OperationBoundary;

public interface operationsService {
public Object invokeOperation(OperationBoundary operation);
public OperationBoundary invokeAsynchronousOperation(OperationBoundary operation);
@Deprecated public List <OperationBoundary> getAllOperations(String userSpace,String userEmail);
public void deleteAllOperations(String userSpace,String userEmail);


}
