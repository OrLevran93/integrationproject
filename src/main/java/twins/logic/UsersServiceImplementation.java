package twins.logic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import twins.controllers.UserBoundary;
import twins.dao.UsersDao;
import twins.data.ItemEntity;
import twins.data.UserEntity;
import twins.data.UserRole;

@Service
public class UsersServiceImplementation implements AdvancedUsersService{
	private String spaceName;
	private UsersDao usersDao;
	private AtomicLong atomicLong; 

	public UsersServiceImplementation() {
	
	}
	@Value("${spring.application.name}")
	public void setSpaceName(String spaceName) {
		this.spaceName = spaceName;
	}

	@Autowired
	public UsersServiceImplementation(UsersDao usersDao) {
		super();
		this.usersDao = usersDao;
		this.atomicLong=new AtomicLong(1L);
	}

	@Override
	@Transactional
	public UserBoundary createUser(UserBoundary user) {
		user.getUserId().setSpace(this.spaceName);
		UserEntity entity= new UserEntity();
		try {
			entity = this.convertFromBoundary(user);
			if( entity.getUsername()== null)
				throw new Exception("Username can not be null");
			if(entity.getAvatar()==null || entity.getAvatar()=="")
				throw new Exception("Avatar can not be null or empty string");
			if (entity.getRole().name()==UserRole.ADMIN.name() || entity.getRole().name()==UserRole.MANAGER.name() || entity.getRole().name()==UserRole.PLAYER.name()) { 

				entity = this.usersDao
						.save(entity);
			}else
				throw new Exception("The role is not valid!");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return this.convertToBoundary(entity);		
	}

    @Override
	public UserBoundary login(String userSpace, String userEmail) {
		UserEntity userEntity = this.usersDao.findOneBySpaceAndEmail(userSpace, userEmail);
        UserBoundary userBoundary = this.convertToBoundary(userEntity);
		return userBoundary;
	}
    

	@Override
	@Transactional
	public UserBoundary updateUser(String userSpace, String userEmail, UserBoundary update) {
		update.getUserId().setSpace(this.spaceName);
		Optional<UserEntity> op = Optional.of(this.usersDao
				.findOneBySpaceAndEmail(userSpace, userEmail));

		if (op.isPresent()) {
			UserEntity existing = op.get();

			UserEntity entity = this.convertFromBoundary(update);
			entity.setAvatar(existing.getAvatar());
			entity.setKey(existing.getKey());
			entity.setRole(existing.getRole());
			entity.setUsername(existing.getUsername());

			entity = this.usersDao
					.save(entity);

			return this.convertToBoundary(entity);	
		}else {
			throw new RuntimeException();  
		}
	}

	@Override
	@Transactional(readOnly = true)
	public List<UserBoundary> getAllUsers(String adminSpace, String adminEmail) {
		/*UserEntity userEntity = this.usersDao.findByEmailAndSpace(adminSpace, adminEmail);
		if(userEntity.getRole().equals(UserRole.ADMIN)) {
			Iterable<UserEntity> allUsers= this.usersDao
					.findAll(); 
			List<UserBoundary> userBoundaries=new ArrayList<>();
			for (UserEntity userEn : allUsers) {
				UserBoundary userBoundary = convertToBoundary(userEn);

				userBoundaries.add(userBoundary);
			}
			return userBoundaries;
		}
		else {
			throw new RuntimeException("you are not an ADMIN");
		}*/
		throw new RuntimeException("deprecated Operation - use the new API getAllUsers(String adminSpace,String adminEmail,int size,int page)");
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<UserBoundary> getAllUsers(String adminSpace, String adminEmail, int size, int page) {
		UserEntity userEntity = this.usersDao.findOneBySpaceAndEmail(adminSpace, adminEmail);
		if(userEntity.getRole().equals(UserRole.ADMIN)) {
			Page<UserEntity> pageOfEntities = this.usersDao.findAll(PageRequest.of(page, size,Direction.ASC,"space"));
			List<UserEntity> entities = pageOfEntities.getContent();
			List<UserBoundary> userBoundaries=new ArrayList<>();
			for (UserEntity userEn : entities) {
				UserBoundary userBoundary = convertToBoundary(userEn);

				userBoundaries.add(userBoundary);
			}
			return userBoundaries;
		}
		else {
			throw new RuntimeException("you are not an ADMIN");
		}
	}

	@Override
	@Transactional
	public void deleteAllUsers(String adminSpace, String adminEmail) {
		UserEntity userEntity = this.usersDao.findOneBySpaceAndEmail(adminSpace, adminEmail);
		if(userEntity.getRole().equals(UserRole.ADMIN)) {
			this.usersDao
			.deleteAll();
		}
		else {
			throw new RuntimeException("you are not an ADMIN");
		}
	}
	
	private UserBoundary convertToBoundary(UserEntity userEntity) {
		UserBoundary userBoundary = new UserBoundary();
 	   if(userBoundary.getUserId()!=null) {
 	   userBoundary.getUserId().setEmail(userEntity.getEmail());
 	   userBoundary.getUserId().setSpace(userEntity.getSpace());
 	   }
        userBoundary.setRole(userEntity.getRole().name());
        userBoundary.setAvatar(userEntity.getAvatar());
        userBoundary.setUsername(userEntity.getUsername());
		return userBoundary;
	}
	
	private UserEntity convertFromBoundary(UserBoundary userBoundary) {
		UserEntity entity = new UserEntity();
		
		entity.setSpace(userBoundary.getUserId().getSpace());
		entity.setEmail(userBoundary.getUserId().getEmail());
		entity.setAvatar(userBoundary.getAvatar());
		entity.setRole(UserRole.valueOf(userBoundary.getRole()));
		entity.setUsername(userBoundary.getUsername());
	
		return entity;	
		
	}
	
}
