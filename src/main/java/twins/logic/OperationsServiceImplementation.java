package twins.logic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import twins.controllers.CreatedBy;
import twins.controllers.InvokedBy;
import twins.controllers.Item;
import twins.controllers.ItemBoundary;
import twins.controllers.Location;
import twins.controllers.OperationBoundary;
import twins.controllers.SpaceId;
import twins.controllers.UserId;
import twins.dao.ItemsDao;
import twins.dao.OperationsDao;
import twins.dao.UsersDao;
import twins.data.ItemEntity;
import twins.data.OperationEntity;
import twins.data.UserEntity;
import twins.data.UserRole;


@Service
public class OperationsServiceImplementation implements AdvancedOperationsService{
	private String spaceName;
	private OperationsDao operationsDao; 
	private UsersDao usersDao;
	private ItemsDao itemsDao;
	private ObjectMapper jackson;
	private AtomicLong atomicLong;
	
	public OperationsServiceImplementation() {
		
	}
	@Value("${spring.application.name}")
	public void setSpaceName(String spaceName) {
		this.spaceName = spaceName;
	}
	@Autowired
	public OperationsServiceImplementation(OperationsDao operationsDao,UsersDao usersDao,ItemsDao itemsDao) {
		super();
		this.operationsDao = operationsDao;
		this.usersDao = usersDao;
		this.itemsDao = itemsDao;
		this.jackson = new ObjectMapper();
		this.atomicLong=new AtomicLong(1L);
	}

	@Override
	public Object invokeOperation(OperationBoundary operation) {
		operation.getOperationId().setId("" + this.atomicLong.getAndIncrement());
		operation.setCreatedTimeStamp(new Date());
		operation.getOperationId().setSpace(this.spaceName);
		UserEntity userEntity = this.usersDao.findOneBySpaceAndEmail(operation.getInvokedBy().getUserId().getSpace(), operation.getInvokedBy().getUserId().getEmail());
		if(userEntity.getRole().equals(UserRole.PLAYER)) {
			ItemEntity itemEntity = this.itemsDao.findOneByItemSpaceAndItemId(operation.getItem().getItemId().getSpace(), operation.getItem().getItemId().getId());
			if(itemEntity!=null) {
				OperationEntity entity= this.convertFromBoundary(operation);
			//	entity.setOperationId("" + this.atomicLong.getAndIncrement());
			//	entity.setCreatedTimeStamp(new Date());
	
				//ItemEntity item = this.itemsDao.findOneByItemSpaceAndItemId(entity.getItemSpace(),entity.getItemId());
				OperationBoundary newOperBoun = operation;
				OperationEntity newOperEntity = null;
				switch(entity.getType()) {
				case "move":
				//	OperationBoundary newOperBoun = operation;
					double x=(double)operation.getOperationsAttributes().get("lat");
					double y=(double)operation.getOperationsAttributes().get("lng");
					newOperBoun.getOperationsAttributes().clear();
					newOperBoun.getOperationsAttributes().put("lat", itemEntity.getLat());
					newOperBoun.getOperationsAttributes().put("lng", itemEntity.getLng());
					newOperBoun.setCreatedTimeStamp(new Date());
					newOperEntity= this.convertFromBoundary(newOperBoun);
					this.operationsDao.save(newOperEntity);
					
					itemEntity.setLat(x);
					itemEntity.setLng(y);
					this.itemsDao.save(itemEntity);
					return this.convertToBoundary(itemEntity);
					//break;
				case "changeStatusToCorona":
					
					  newOperBoun.getOperationsAttributes().clear();
					  newOperBoun.getOperationsAttributes().put("isInfected", true);
					  newOperBoun.getOperationsAttributes().put("infectionDate", entity.getCreatedTimeStamp());
					  newOperEntity= this.convertFromBoundary(newOperBoun);
					  this.operationsDao.save(newOperEntity);
					ItemBoundary newItemBounCo = this.convertToBoundary(itemEntity);
					Boolean isInfectedCo = (Boolean)newItemBounCo.getItemAttributes().get("isInfected");
					if(!isInfectedCo) {
					newItemBounCo.getItemAttributes().clear();
					newItemBounCo.getItemAttributes().put("isInfected",true);
					newItemBounCo.getItemAttributes().put("infectionDate",new Date());
					}
					ItemEntity newItemEntityCo = this.convertFromBoundary(newItemBounCo);	
					this.itemsDao.save(newItemEntityCo);
					return this.convertToBoundary(newItemEntityCo);
					// code block
					//break;
				case "changeStatusToClear":
                      newOperBoun.getOperationsAttributes().clear();
					  newOperBoun.getOperationsAttributes().put("isInfected",false);
					  newOperBoun.getOperationsAttributes().put("clearingDate",entity.getCreatedTimeStamp());
					  newOperEntity= this.convertFromBoundary(newOperBoun);
					  this.operationsDao.save(newOperEntity);
					ItemBoundary newItemBoun = this.convertToBoundary(itemEntity);
					Boolean isInfected = (Boolean)newItemBoun.getItemAttributes().get("isInfected");
					if(isInfected) {
						newItemBoun.getItemAttributes().clear();
						newItemBoun.getItemAttributes().put("isInfected",false);
	
						newItemBoun.getItemAttributes().put("clearingDate",entity.getCreatedTimeStamp());
					}
					ItemEntity newItemEntity = this.convertFromBoundary(newItemBoun);
					this.itemsDao.save(newItemEntity);
					return this.convertToBoundary(newItemEntity);
					//break;
				default:
					// code block
					entity=this.operationsDao.save(entity);
					return this.convertToBoundary(entity).getOperationsAttributes();
				}

			}else {
				throw new RuntimeException("itemSpace: "+ itemEntity.getItemSpace()+" or itemId "+itemEntity.getItemSpace()+" NOT exists" );
			}

		}else {
			throw new RuntimeException("you are not a PLAYER");
		}

	}


	@Override
	public OperationBoundary invokeAsynchronousOperation(OperationBoundary operation) {
		operation.getOperationId().setSpace(this.spaceName);
		UserEntity userEntity = this.usersDao.findOneBySpaceAndEmail(operation.getInvokedBy().getUserId().getSpace(), operation.getInvokedBy().getUserId().getEmail());
		if(userEntity.getRole().equals(UserRole.PLAYER)) {
			ItemEntity itemEntity = this.itemsDao.findOneByItemSpaceAndItemId(operation.getItem().getItemId().getSpace(), operation.getItem().getItemId().getId());
			if(itemEntity!=null) {
				OperationEntity entity = new OperationEntity();
				entity = this.convertFromBoundary(operation);
				entity.setOperationId("" + this.atomicLong.getAndIncrement());
				entity.setCreatedTimeStamp(new Date());
				return this.convertToBoundary(entity);
			}else {
				throw new RuntimeException("itemSpace: "+ itemEntity.getItemSpace()+" or itemId "+itemEntity.getItemSpace()+" NOT exists" );
			}
			
		}else {
			throw new RuntimeException("you are not a PLAYER");
		}

	}

	@Override
	@Transactional(readOnly = true)
	public List<OperationBoundary> getAllOperations(String userSpace, String userEmail) {
		UserEntity userEntity = this.usersDao.findOneBySpaceAndEmail(userSpace, userEmail);
		if(userEntity.getRole().equals(UserRole.ADMIN)) {
			Iterable<OperationEntity> allOperations= this.operationsDao
					.findAll(); 
			List<OperationBoundary> operationBoundaries=new ArrayList<>();
			for (OperationEntity operationEntity : allOperations) {
				OperationBoundary operationBoundary = convertToBoundary(operationEntity);

				operationBoundaries.add(operationBoundary);
			}
			return operationBoundaries;
		}
		else {
			throw new RuntimeException("you are not an ADMIN");
			
		}
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<OperationBoundary> getAllOperations(String userSpace, String userEmail, int size, int page) {
		UserEntity userEntity = this.usersDao.findOneBySpaceAndEmail(userSpace, userEmail);
		if(userEntity.getRole().equals(UserRole.ADMIN)) {
			Page<OperationEntity> pageOfEntities = this.operationsDao.findAll(PageRequest.of(page, size, Direction.ASC,"operationId"));
			List<OperationEntity> entities = pageOfEntities.getContent();
			List<OperationBoundary> operationBoundaries=new ArrayList<>();
			for (OperationEntity operationEntity : entities) {
				OperationBoundary operationBoundary = convertToBoundary(operationEntity);

				operationBoundaries.add(operationBoundary);
			}
			return operationBoundaries;	
		}
		else {
			throw new RuntimeException("you are not an ADMIN");
		}
	}


	@Override
	public void deleteAllOperations(String userSpace, String userEmail) {
		UserEntity userEntity = this.usersDao.findOneBySpaceAndEmail(userSpace, userEmail);
		if(userEntity.getRole().equals(UserRole.ADMIN)) {
			this.operationsDao.deleteAll();
		}
		else {
			throw new RuntimeException("you are not an ADMIN");
		}
	}


	private OperationEntity convertFromBoundary(OperationBoundary operation) {
		OperationEntity entity= new OperationEntity();
		if(operation.getOperationId()!=null) {
			entity.setOperationSpace(operation.getOperationId().getSpace());
			entity.setOperationId(operation.getOperationId().getId());
		}
		entity.setType(operation.getType());
		if(operation.getItem()!=null) {
			if(operation.getItem().getItemId()!=null) {
				entity.setItemSpace(operation.getItem().getItemId().getSpace());
				entity.setItemId(operation.getItem().getItemId().getId());
			}
		}
		entity.setCreatedTimeStamp(operation.getCreatedTimeStamp());
		if(operation.getInvokedBy()!=null) {
			if(operation.getInvokedBy().getUserId()!=null) {
				entity.setUserSpace(operation.getInvokedBy().getUserId().getSpace());
				entity.setUserEmail(operation.getInvokedBy().getUserId().getEmail());
			}
		}
		String json = this.marshal(operation.getOperationsAttributes());
		entity.setOperationsAttributes(json);
		return entity;
		
		
	}
	

	private OperationBoundary convertToBoundary(OperationEntity operationEntity) {
		OperationBoundary oper = new OperationBoundary();
		
		SpaceId id= new SpaceId();
		id.setId(operationEntity.getOperationId());
		id.setSpace(operationEntity.getOperationSpace());
		oper.setOperationId(id);
		oper.setType(operationEntity.getType());
		Item item = new Item();
		SpaceId itemId = new SpaceId();
		itemId.setId(operationEntity.getItemId());
		itemId.setSpace(operationEntity.getItemSpace());
		item.setItemId(itemId);
		oper.setItem(item);
		UserId user = new UserId();
		user.setEmail(operationEntity.getUserEmail());
		user.setSpace(operationEntity.getUserSpace());
		InvokedBy invoke = new InvokedBy();
		invoke.setUserId(user);
		oper.setInvokedBy(invoke);
		oper.setOperationsAttributes((Map<String, Object>)this.unmarshal(operationEntity.getOperationsAttributes(), Map.class));
		return oper;
	}
	
	private ItemEntity convertFromBoundary(ItemBoundary item) {
		ItemEntity entity = new ItemEntity();
		entity.setType(item.getType());
		entity.setName(item.getName());
		entity.setActive(item.getActive());
		entity.setLng(item.getLocation().getLng());
		entity.setLat(item.getLocation().getLat());
		entity.setKey(item.getItemId().getSpace()+"="+item.getItemId().getId());
		entity.setItemSpace(item.getItemId().getSpace());
		entity.setItemId(item.getItemId().getId());
		if(item.getLocation()!=null) {
			entity.setLng(item.getLocation().getLng());
			entity.setLat(item.getLocation().getLat());
		}
		if(item.getCreatedBy()!=null) {
			if(item.getCreatedBy().getUserId()!=null) {
				entity.setCreatorSpace(item.getCreatedBy().getUserId().getSpace());
				entity.setCreatorEmail(item.getCreatedBy().getUserId().getEmail());
			}
		}
		entity.setCreatedTimeStamp(item.getCreatedTimeStamp());
		//don't forget about the active
		String json = this.marshal(item.getItemAttributes());
		entity.setItemAttributes(json);

		
		return entity;
	}
	

	private ItemBoundary convertToBoundary(ItemEntity entity) {
		ItemBoundary item = new ItemBoundary();
		item.setType(entity.getType());
		item.setName(entity.getName());
		item.setActive(entity.getActive());
		Location location = new Location();
		location.setLat(entity.getLat());
		location.setLng(entity.getLng());
		item.setLocation(location);
		SpaceId id = new SpaceId();
		id.setSpace(entity.getItemSpace());
		id.setId(entity.getItemId());
		item.setItemId(id);
		item.setItemAttributes((Map<String, Object>)this.unmarshal(entity.getItemAttributes(), Map.class));
		item.setCreatedTimeStamp(entity.getCreatedTimeStamp());
		UserId user=new UserId();
		user.setSpace(entity.getCreatorSpace());
		user.setEmail(entity.getCreatorEmail());
		CreatedBy creator=new CreatedBy();
		creator.setUserId(user);
		item.setCreatedBy(creator);
		//Dont forget about the active
		return item;
	}
	
	private <T> T unmarshal(String json, Class<T> type) {
		try {
			return this.jackson
				.readValue(json, type);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private String marshal(Object moreDetails) {
		try {
			return this.jackson
					.writeValueAsString(moreDetails);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
