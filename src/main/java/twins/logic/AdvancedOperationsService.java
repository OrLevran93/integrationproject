package twins.logic;

import java.util.List;

import twins.controllers.OperationBoundary;

public interface AdvancedOperationsService extends operationsService {
	
	public List <OperationBoundary> getAllOperations(String userSpace,String userEmail,int size,int page);
}
