package twins.logic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import twins.controllers.CreatedBy;
import twins.controllers.ItemBoundary;
import twins.controllers.Location;
import twins.controllers.SpaceId;
import twins.controllers.UserId;
import twins.dao.ItemsDao;
import twins.dao.UsersDao;
import twins.data.ItemEntity;
import twins.data.UserEntity;
import twins.data.UserRole;

@Service
public class ItemsServiceImplementation implements AdvancedItemsService{
	private String spaceName;
	private ItemsDao itemsDao;
	private UsersDao usersDao;
	private AtomicLong atomicLong;
	private ObjectMapper jackson;
	
	public ItemsServiceImplementation() {
		
	}
	
	@Value("${spring.application.name}")
	public void setSpaceName(String spaceName) {
		this.spaceName = spaceName;
	}

	@Autowired
	public ItemsServiceImplementation(ItemsDao itemsDao,UsersDao usersDao) {
		super();
		this.itemsDao = itemsDao;
		this.usersDao = usersDao;
		this.jackson = new ObjectMapper();
		this.atomicLong=new AtomicLong(1L);
	}
	

	@Override
	public ItemBoundary createItem(String userSpace,String userEmail,ItemBoundary item) {
		if(item.getItemId()==null) {
			SpaceId itemId = new SpaceId();
		}
		item.getItemId().setSpace(this.spaceName);
		UserEntity userEntity = this.usersDao.findOneBySpaceAndEmail(userSpace, userEmail);
			if(userEntity.getRole().equals(UserRole.MANAGER)) {
				if(item.getCreatedBy()==null) {
					CreatedBy newCreateBy = new CreatedBy();
					UserId newUserId = new UserId();
					newUserId.setEmail(userEmail);
					newUserId.setSpace(userSpace);
					newCreateBy.setUserId(newUserId);
					item.setCreatedBy(newCreateBy);
				}
			ItemEntity entity = this.convertFromBoundary(item);
			entity.setItemId("" + this.atomicLong.getAndIncrement());
			entity.setCreatedTimeStamp(new Date());
			entity = this.itemsDao
				.save(entity);
			return this.convertToBoundary(entity);
		}
		else {
			throw new RuntimeException("you are not a MANAGER");
		}
	}


	@Override
	public ItemBoundary updateItem(String userSpace, String userEmail, String itemSpace, String itemId,
			ItemBoundary update) {
		update.getItemId().setSpace(this.spaceName);
		UserEntity userEntity = this.usersDao.findOneBySpaceAndEmail(userSpace, userEmail);
		if(userEntity.getRole().equals(UserRole.MANAGER)) {
			Optional<ItemEntity> op = Optional.of(this.itemsDao
					.findOneByItemSpaceAndItemId(itemSpace,itemId));
			
			if(op.isPresent()) {
				ItemEntity existing = op.get();
				ItemEntity updated = this.convertFromBoundary(update);
				
				existing.setType(updated.getType());
				existing.setActive(updated.getActive());
				existing.setName(updated.getName());
				existing.setLng(updated.getLng());
				existing.setLat(updated.getLat());
				existing.setKey(updated.getKey());
				existing.setItemAttributes(updated.getItemAttributes());
				this.itemsDao
						.save(existing);

				return this.convertToBoundary(existing);	
			}else {
				throw new RuntimeException("itemSpace: "+itemSpace+" or itemId "+itemId+" NOT exists" );
			}
		}
		else {
			throw new RuntimeException("you are not a MANAGER");
		}
	}

	@Override
	@Transactional(readOnly = true)
	public List<ItemBoundary> getAllItems(String userSpace, String userEmail) {
		UserEntity userEntity = this.usersDao.findOneBySpaceAndEmail(userSpace, userEmail);
		if(userEntity.getRole().equals(UserRole.MANAGER) || userEntity.getRole().equals(UserRole.PLAYER)) {
			Iterable<ItemEntity> allItems= this.itemsDao
					.findAll(); 
			List<ItemBoundary> itemBoundaries=new ArrayList<>();
			for (ItemEntity itemEntity : allItems) {
				ItemBoundary itemBoundary = convertToBoundary(itemEntity);
				//hiding all items that not active from player
				if(itemBoundary.getActive()== true || userEntity.getRole().equals(UserRole.MANAGER)) {
					itemBoundaries.add(itemBoundary);
				}	
			}
			return itemBoundaries;
		}
		else {
			throw new RuntimeException("you are not a MANAGER or a PLAYER");
		}
	//	throw new RuntimeException("deprecated Operation - use the new API getAllItems(String userSpace, String userEmail,size,page)");
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<ItemBoundary> getAllItems(String userSpace, String userEmail, int size, int page) {
		UserEntity userEntity = this.usersDao.findOneBySpaceAndEmail(userSpace, userEmail);
		if(userEntity.getRole().equals(UserRole.MANAGER) || userEntity.getRole().equals(UserRole.PLAYER)) {
			Page<ItemEntity> pageOfEntities = this.itemsDao.findAll(PageRequest.of(page, size, Direction.ASC,"itemId"));

			List<ItemEntity> entities = pageOfEntities.getContent();
			List<ItemBoundary> itemBoundaries=new ArrayList<>();
			for (ItemEntity itemEntity : entities) {
				ItemBoundary itemBoundary = convertToBoundary(itemEntity);
				//hiding all items that not active from player
				if(itemBoundary.getActive()== true || userEntity.getRole().equals(UserRole.MANAGER)) {
					itemBoundaries.add(itemBoundary);
				}	
			}
			return itemBoundaries;
		}
		else {
			throw new RuntimeException("you are not a MANAGER or a PLAYER");
		}
	}
	

	@Override
	@Transactional
	public void deleteAllItems(String userSpace, String userEmail) {
		UserEntity userEntity = this.usersDao.findOneBySpaceAndEmail(userSpace, userEmail);
		if(userEntity.getRole().equals(UserRole.ADMIN)) {
			this.itemsDao
			.deleteAll();
		}
		else {
			throw new RuntimeException("you are not an ADMIN");
		}
	}

	@Override
	public ItemBoundary getSpecifitItem(String userSpace, String userEmail, String itemSpace, String itemId) {
		UserEntity userEntity = this.usersDao.findOneBySpaceAndEmail(userSpace, userEmail);
		if(userEntity.getRole().equals(UserRole.MANAGER) || userEntity.getRole().equals(UserRole.PLAYER)) {
			Optional<ItemEntity> op = Optional.of(this.itemsDao
					.findOneByItemSpaceAndItemId(itemSpace,itemId));
			
			if(op.isPresent()) {
				ItemEntity existing = op.get();
				if(userEntity.getRole().equals(UserRole.PLAYER)) {
					if(existing.getActive()==false) {
						throw new RuntimeException("you are not a MANAGER");
					}
				}
				return this.convertToBoundary(existing);
			}else
			 {
				throw new RuntimeException("itemSpace: "+itemSpace+" or itemId "+itemId+" NOT exists" );
			}
		}
		else {
			throw new RuntimeException("you are not a MANAGER or a PLAYER");
		}


	}
	

	private ItemEntity convertFromBoundary(ItemBoundary item) {
		ItemEntity entity = new ItemEntity();
		entity.setType(item.getType());
		entity.setName(item.getName());
		entity.setActive(item.getActive());
		entity.setLng(item.getLocation().getLng());
		entity.setLat(item.getLocation().getLat());
		entity.setKey(item.getItemId().getSpace()+"="+item.getItemId().getId());
		entity.setItemSpace(item.getItemId().getSpace());
		entity.setItemId(item.getItemId().getId());
		if(item.getLocation()!=null) {
			entity.setLng(item.getLocation().getLng());
			entity.setLat(item.getLocation().getLat());
		}
		if(item.getCreatedBy()!=null) {
			if(item.getCreatedBy().getUserId()!=null) {
				entity.setCreatorSpace(item.getCreatedBy().getUserId().getSpace());
				entity.setCreatorEmail(item.getCreatedBy().getUserId().getEmail());
			}
		}
		entity.setCreatedTimeStamp(item.getCreatedTimeStamp());
		//don't forget about the active
		String json = this.marshal(item.getItemAttributes());
		entity.setItemAttributes(json);

		
		return entity;
	}
	

	private ItemBoundary convertToBoundary(ItemEntity entity) {
		ItemBoundary item = new ItemBoundary();
		item.setType(entity.getType());
		item.setName(entity.getName());
		item.setActive(entity.getActive());
		Location location = new Location();
		location.setLat(entity.getLat());
		location.setLng(entity.getLng());
		item.setLocation(location);
		SpaceId id = new SpaceId();
		id.setSpace(entity.getItemSpace());
		id.setId(entity.getItemId());
		item.setItemId(id);
		item.setItemAttributes((Map<String, Object>)this.unmarshal(entity.getItemAttributes(), Map.class));
		item.setCreatedTimeStamp(entity.getCreatedTimeStamp());
		UserId user=new UserId();
		user.setSpace(entity.getCreatorSpace());
		user.setEmail(entity.getCreatorEmail());
		CreatedBy creator=new CreatedBy();
		creator.setUserId(user);
		item.setCreatedBy(creator);
		//Dont forget about the active
		return item;
	}

	private <T> T unmarshal(String json, Class<T> type) {
		try {
			return this.jackson
				.readValue(json, type);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private String marshal(Object moreDetails) {
		try {
			return this.jackson
					.writeValueAsString(moreDetails);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
