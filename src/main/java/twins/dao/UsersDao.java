package twins.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import twins.data.UserEntity;

public interface UsersDao extends PagingAndSortingRepository<UserEntity,String>{


	
//	@Query(value="SELECT u FROM USERS u WHERE  u.space = ?1 and u.email = ?2 ", nativeQuery = true)
	public UserEntity findOneBySpaceAndEmail(String userSpace, String userEmail);
	
	
	public void deleteByEmailAndSpace(String adminSpace, String adminEmail);
	
	
}