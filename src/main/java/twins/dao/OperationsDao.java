package twins.dao;

import org.springframework.data.repository.CrudRepository;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import twins.data.OperationEntity;
public interface OperationsDao extends PagingAndSortingRepository<OperationEntity, String>{



}
