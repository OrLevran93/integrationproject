package twins.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import twins.data.ItemEntity;
//@Repository
public interface ItemsDao extends PagingAndSortingRepository<ItemEntity, String>{

//CrudRepository<ItemEntity, String> {



	//need to be reference to item table NOT users
	//@Query(value="SELECT u FROM USERS u WHERE  u.space = ?1 and u.email = ?2 ", nativeQuery = true)
	public ItemEntity findOneByItemSpaceAndItemId(String itemSpace,String itemId);
	
	public void deleteByCreatorEmailAndCreatorSpace( @Param ("creatorSpace")String creatorSpace, @Param ("creatorEmail") String creatorEmail);
}
