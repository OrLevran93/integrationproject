package twins.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import twins.logic.AdvancedOperationsService;
import twins.logic.AdvancedUsersService;
import twins.logic.ItemsService;



@RestController
public class AdminController {

	private AdvancedUsersService usersService;
	private ItemsService itemsService;
	private AdvancedOperationsService operationsService;
	@Autowired
	public void setUsersService(AdvancedUsersService usersService) {
		this.usersService = usersService;
	}
	@Autowired
	public void setItemsService(ItemsService itemsService) {
		this.itemsService = itemsService;
	}
	@Autowired
	public void setOperationsService(AdvancedOperationsService operationsService) {
		this.operationsService = operationsService;
	}

	@RequestMapping(
			path = "/twins/admin/users/{userSpace}/{userEmail}",
			method = RequestMethod.DELETE)
	public void deleteUsers (@PathVariable("userSpace") String space,@PathVariable("userEmail") String email) {
		this.usersService.deleteAllUsers(space, email);
	}

	@RequestMapping(
			path = "/twins/admin/items/{userSpace}/{userEmail}",
			method = RequestMethod.DELETE)
	public void deleteItems (@PathVariable("userSpace") String space,@PathVariable("userEmail") String email) {
		this.itemsService.deleteAllItems(space, email);
	}
	@RequestMapping(
			path = "/twins/admin/operations/{userSpace}/{userEmail}",
			method = RequestMethod.DELETE)
	public void deleteOperations (@PathVariable("userSpace") String space,@PathVariable("userEmail") String email) {
		this.operationsService.deleteAllOperations(space, email);
	}

	@RequestMapping(path = "/twins/admin/users/{userSpace}/{userEmail}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public UserBoundary[] exportUsers(@PathVariable("userSpace") String space,@PathVariable("userEmail") String email,
			@RequestParam(name="size",required=false,defaultValue="5") int size,
			@RequestParam(name="page",required=false,defaultValue="0") int page) {
		List<UserBoundary> users = this.usersService.getAllUsers(space, email,size,page);
		//UserBoundary[] users= (UserBoundary[]) this.usersService.getAllUsers(space, email).toArray();
		return users.toArray(new UserBoundary[0]);
	}

	@RequestMapping(path = "/twins/admin/operations/{userSpace}/{userEmail}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public OperationBoundary[] exportOperstions(@PathVariable("userSpace") String space,@PathVariable("userEmail") String email)
//			@RequestParam(name="size",required=false,defaultValue="5") int size,
//			@RequestParam(name="page",required=false,defaultValue="0") int page)
			{
		List<OperationBoundary> operations = this.operationsService.getAllOperations(space, email);
			//	,size,page);
		//OperationBoundary[] operations= (OperationBoundary[]) this.operationsService.getAllOperations(space, email).toArray();
		return operations.toArray(new OperationBoundary[0]);
	}
}
