package twins.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import twins.logic.UsersService;

@RestController
public class UserController {
	private UsersService usersService;

	@Autowired
	public void setUsersService(UsersService usersService) {
		this.usersService = usersService;
	}

	@RequestMapping(path = "/twins/users/login/{userSpace}/{userEmail}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public UserBoundary login(@PathVariable("userSpace") String space,@PathVariable("userEmail") String email) {
		
		return this.usersService.login(space, email);
	}

	@RequestMapping(
			path = "/twins/users",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public UserBoundary createUser (@RequestBody NewUserDetails input){
		UserBoundary userInput = new UserBoundary();
		UserId userId = new UserId();
		userId.setEmail(input.getEmail());
		userInput.setUserId(userId);
		userInput.setUsername(input.getUsername());
		userInput.setRole(input.getRole());
		userInput.setAvatar(input.getAvatar());
		return this.usersService.createUser(userInput);
	}

	@RequestMapping(
			path="/twins/users/{userSpace}/{userEmail}",
			method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public void updateUser (@PathVariable("userSpace") String space,@PathVariable("userEmail") String email, 
			@RequestBody UserBoundary update) {
		this.usersService.updateUser(space, email, update);
	}



}
