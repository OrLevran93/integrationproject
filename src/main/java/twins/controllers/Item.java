package twins.controllers;

public class Item {

	private SpaceId itemId;

	public Item() {
		super();
	}

	public SpaceId getItemId() {
		return itemId;
	}

	public void setItemId(SpaceId itemId) {
		this.itemId = itemId;
	}

}
