package twins.controllers;



import org.springframework.web.bind.annotation.RestController;

import twins.logic.AdvancedItemsService;
//import twins.logic.ItemsService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
public class ItemController {
	private AdvancedItemsService itemsService;

	@Autowired
	public void setItemsService(AdvancedItemsService itemsService) {
		this.itemsService = itemsService;
	}

    @RequestMapping(
            path = "/twins/items/{userSpace}/{userEmail}",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)

    public ItemBoundary createItem(@PathVariable("userSpace") String space,@PathVariable("userEmail") String email,@RequestBody ItemBoundary input) {

    	return this.itemsService.createItem(space, email, input);

    }
    @RequestMapping(
			path="/twins/items/{userSpace}/{userEmail}/{itemSpace}/{itemId}",
			method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE)
    //was void changed to ItemBoundary
    public void updateItem(@PathVariable("userSpace") String userSpace,@PathVariable("userEmail") String email,@PathVariable("itemSpace") String space,@PathVariable("itemId") String itemId,@RequestBody ItemBoundary update) {
    	  this.itemsService.updateItem(userSpace, email, space, itemId, update);
    }

    @RequestMapping(
    		path = "/twins/items/{userSpace}/{userEmail}/{itemSpace}/{itemId}", 
    		method = RequestMethod.GET, 
    		produces = MediaType.APPLICATION_JSON_VALUE)
	public ItemBoundary retrieveItem(@PathVariable("userSpace") String userSpace,@PathVariable("userEmail") String email,@PathVariable("itemSpace") String space,@PathVariable("itemId") String itemId) {
    	return this.itemsService.getSpecifitItem(userSpace, email, space, itemId);
    	//ItemBoudnary item = new ItemBoudnary();
    	//return item;
	}
    
    @RequestMapping(
    		path = "/twins/items/{userSpace}/{userEmail}",
    		method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE)
    //changed from ItemBoudnary[] to List<ItemBoundary>
	public List<ItemBoundary> getAllItems(@PathVariable("userSpace") String userSpace,@PathVariable("userEmail") String email)
//			@RequestParam(name="size",required=false,defaultValue="5") int size,
//			@RequestParam(name="page",required=false,defaultValue="0") int page)
			{
		return this.itemsService.getAllItems(userSpace, email);
				//,size,page);
	}
    
}