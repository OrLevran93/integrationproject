package twins.controllers;

import java.util.Date;
import java.util.Map;

public class OperationBoundary {
	
	private SpaceId operationId;
	private String type;
	private Item item;
	private Date createdTimestamp;
	private InvokedBy invokedBy;
	private Map<String,Object> operationsAttributes;
	
	public OperationBoundary() {

	}

	public SpaceId getOperationId() {
		return operationId;
	}

	public void setOperationId(SpaceId operationId) {
		this.operationId = operationId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Date getCreatedTimeStamp() {
		return createdTimestamp;
	}

	public void setCreatedTimeStamp(Date createdTimeStamp) {
		this.createdTimestamp = createdTimeStamp;
	}

	public InvokedBy getInvokedBy() {
		return invokedBy;
	}

	public void setInvokedBy(InvokedBy invokedBy) {
		this.invokedBy = invokedBy;
	}

	public Map<String, Object> getOperationsAttributes() {
		return operationsAttributes;
	}

	public void setOperationsAttributes(Map<String, Object> operationsAttributes) {
		this.operationsAttributes = operationsAttributes;
	}

	

}

