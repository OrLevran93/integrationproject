package twins.controllers;

public class SpaceId {
	private String space;
	private String id;
	
	public SpaceId() {
		super();
	}

	public String getSpace() {
		return space;
	}

	public void setSpace(String space) {
		this.space = space;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
