package twins.controllers;


import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import net.minidev.json.JSONObject;
import twins.logic.operationsService;
@RestController
public class OperationController {
	private operationsService service;

	@Autowired
	public void setOperationsService(operationsService service) {
		this.service = service;
	}
	
	@RequestMapping(
			path = "/twins/operations",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public Object invokeOperationOnItem (@RequestBody OperationBoundary input){
		//return JSONOBJECT
		Object obj= this.service.invokeOperation(input);
	//	JSONObject jsonobject = (JSONObject) obj;
		return obj;
	}
	
	@RequestMapping(
			path = "/twins/operations/async",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public OperationBoundary AsynchronousOpration(@RequestBody OperationBoundary input){
		//return A-synchronous Operation with null operationId 
		//OperationBoundary op = new OperationBoundary();
		//op.setOperationId(null);
		return this.service.invokeAsynchronousOperation(input);
	}
}
