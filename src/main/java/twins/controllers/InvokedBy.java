package twins.controllers;

public class InvokedBy {
	
	private UserId userId;

	public InvokedBy() {
		super();
	}

	public UserId getUserId() {
		return userId;
	}

	public void setUserId(UserId userId) {
		this.userId = userId;
	}
	
}
