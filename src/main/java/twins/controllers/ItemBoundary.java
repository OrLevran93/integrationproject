package twins.controllers;

import java.util.Date;
import java.util.Map;

public class ItemBoundary {
	
	private SpaceId itemId;
	private String type;
	private String name;
	private boolean active;
	private Date createdTimestamp;
	private CreatedBy createdBy;
	private Location location;
	private Map<String,Object> itemAttributes;
	
	public ItemBoundary() {

	}

	public SpaceId getItemId() {
		return itemId;
	}

	public void setItemId(SpaceId itemId) {
		this.itemId = itemId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getCreatedTimeStamp() {
		return createdTimestamp;
	}

	public void setCreatedTimeStamp(Date createdTimeStamp) {
		this.createdTimestamp = createdTimeStamp;
	}

	public CreatedBy getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(CreatedBy createdBy) {
		this.createdBy = createdBy;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Map<String, Object> getItemAttributes() {
		return itemAttributes;
	}

	public void setItemAttributes(Map<String, Object> itemAttributes) {
		this.itemAttributes = itemAttributes;
	}
}
